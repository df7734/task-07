package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		Team team = new Team();
		team.setName(name);
		team.setId(0);
		return team;
	}

	@Override
	public boolean equals(Object obj) {
		try{
			Team team = (Team) obj;
			if(this.getName().equals(team.getName())){
				return true;
			}
		} catch (Exception ignored){

		}
		return false;
	}

	@Override
	public String toString() {
		return this.getName();
	}
}
