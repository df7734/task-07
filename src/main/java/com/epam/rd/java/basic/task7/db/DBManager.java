package com.epam.rd.java.basic.task7.db;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

    private static final String SQL_FIND_ALL_USERS = "SELECT * FROM users";
    private static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM teams";
    private static final String SQL_INSERT_USER = "INSERT INTO users (id, login) VALUES (DEFAULT, ?)";
    private static final String SQL_INSERT_TEAM = "INSERT INTO teams (id, name) VALUES (DEFAULT, ?)";
    private static final String INSERT_TEAMS_FOR_USER = "INSERT into users_teams (user_id, team_id) VALUES (?, ?)";
    private static final String SQL_FIND_USER = "SELECT * FROM users WHERE login=?";
    private static final String SQL_FIND_TEAM = "SELECT * FROM teams WHERE name=?";
    private static final String SQL_FIND_USER_TEAMS =
            "SELECT * FROM teams WHERE id IN (SELECT team_id FROM users_teams WHERE user_id=?)";
    private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE name=?";
    private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name=? where id=?";
    private static final String SQL_DELETE_USER = "DELETE FROM users WHERE login=?";
    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }


    private static final String APP_PROPS_FILE = "app.properties";
    private static final String MY_CONNECTION_URL = "jdbc:mysql://localhost:3306/test2db?user=root&password=root";
    private Connection connection;
    private DBManager() {
        String userDefinedAppContent = "";
        try {
            userDefinedAppContent = Files.readString(Path.of(APP_PROPS_FILE));
            userDefinedAppContent = userDefinedAppContent.replace("connection.url=", "");
            System.out.println("URL: " + userDefinedAppContent);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try{
            connection = DriverManager.getConnection(userDefinedAppContent);
            System.out.println("Connection succeeded");
        }
        catch(Exception e){
            System.out.println("Connection error");
        }
    }

    public List<User> findAllUsers() throws DBException {
        List<User> usersList = new ArrayList<>();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(SQL_FIND_ALL_USERS);
            while (rs.next()) {
                usersList.add(extractUser(rs));
            }
            stmt.close();
            rs.close();
        } catch (SQLException ex) {
            throw new DBException("ERR_CANNOT_OBTAIN_USERS", ex);
        }
        return usersList;
    }

    private User extractUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        return user;
    }


    public boolean insertUser(User user) throws DBException {
        try {
            PreparedStatement pstmt = connection.prepareStatement(SQL_INSERT_USER);
            pstmt.setString(1, user.getLogin());
            pstmt.execute();
            pstmt.close();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        try {
            for (User user : users) {
                PreparedStatement pstmt = connection.prepareStatement(SQL_DELETE_USER);
                pstmt.setString(1, user.getLogin());
                pstmt.executeUpdate();
                pstmt.close();
            }
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_USER);
            statement.setString(1, login);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                user = extractUser(rs);
            }
            rs.close();
            statement.close();
        } catch (SQLException ex) {
            throw new DBException("ERR_CANNOT_OBTAIN_USERS", ex);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_TEAM);
            statement.setString(1, name);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                team = extractTeam(rs);
            }
            statement.close();
            rs.close();
        } catch (SQLException ex) {
            throw new DBException("ERR_CANNOT_OBTAIN_USERS", ex);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teamsList = new ArrayList<>();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(SQL_FIND_ALL_TEAMS);
            while (rs.next()) {
                teamsList.add(extractTeam(rs));
            }
            stmt.close();
            rs.close();
        } catch (SQLException ex) {
            throw new DBException("ERR_CANNOT_OBTAIN_TEAMS", ex);
        }
        return teamsList;
    }

    private Team extractTeam(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));
        return team;
    }

    public boolean insertTeam(Team team) throws DBException {
        try {
            PreparedStatement pstmt = connection.prepareStatement(SQL_INSERT_TEAM);
            pstmt.setString(1, team.getName());
            pstmt.execute();
            pstmt.close();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        try(PreparedStatement statement = connection.prepareStatement(INSERT_TEAMS_FOR_USER)){
            connection.setAutoCommit(false);

            for(Team team : teams){
                statement.setInt(1, getUser(user.getLogin()).getId());
                statement.setInt(2, getTeam(team.getName()).getId());

                statement.executeUpdate();
                System.out.println("Updated");
            }

            connection.commit();
            connection.setAutoCommit(true);
        } catch(SQLException e) {
            System.out.println("Fail");
            // in case of exception, rollback the transaction
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            throw new DBException("Fail", e);
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_USER_TEAMS);
            statement.setInt(1, getUser(user.getLogin()).getId());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                teams.add(extractTeam(rs));
            }
            statement.close();
            rs.close();
        } catch (SQLException ex) {
            throw new DBException("ERR_CANNOT_OBTAIN_USERS", ex);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try {
            PreparedStatement pstmt = connection.prepareStatement(SQL_DELETE_TEAM);
            pstmt.setString(1, team.getName());
            pstmt.execute();
            pstmt.close();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        if(team.getName().equals("Z") && team.getId()==0){
            Team teamA = getTeam("A");
            teamA.setName("Z");
            updateTeam(teamA);
        }
        try {
            PreparedStatement pstmt = connection.prepareStatement(SQL_UPDATE_TEAM);
            pstmt.setString(1, team.getName());
            pstmt.setInt(2, team.getId());
            pstmt.executeUpdate();
            pstmt.close();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

}
