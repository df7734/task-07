package com.epam.rd.java.basic.task7.db.entity;

public class User {

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {

		User user = new User();
		user.setLogin(login);
		user.setId(0);
		return user;
	}

	@Override
	public boolean equals(Object obj) {
		try{
			User user = (User) obj;
			if(this.getLogin().equals(user.getLogin())){
				return true;
			}
		}
		catch (Exception e){
			return false;
		}
		return false;
	}

    @Override
    public String toString() {
        return this.getLogin();
    }
}
